package com.example.kotlinlivenessdemoapp.ui.theme


import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalView
import androidx.core.view.ViewCompat

val ColorScheme.successContainer: Color
    @Composable
    get() = if (!isSystemInDarkTheme()) Color(0xFFD6F5DB) else Color(0xFF365E3D)

val ColorScheme.onSuccessContainer: Color
    @Composable
    get() = if (!isSystemInDarkTheme()) Color(0xFF365E3D) else Color(0xFFD6F5DB)



@Composable
fun MyApplicationTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) {

    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            ViewCompat.getWindowInsetsController(view)?.isAppearanceLightStatusBars = darkTheme
        }
    }

    MaterialTheme(
        content = content
    )
}