package com.example.kotlinlivenessdemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.biometrics.kotlinlivenesssdk.LivenessMainViewModel
import com.biometrics.kotlinlivenesssdk.ui.LivenessScreen
import com.example.kotlinlivenessdemoapp.ui.HomeScreen
import com.example.kotlinlivenessdemoapp.ui.theme.MyApplicationTheme
import com.example.kotlinlivenessdemoapp.ui.ResultScreen

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                Navigation()
            }
        }
    }

    @Composable
    private fun Navigation(viewModel: LivenessMainViewModel = LivenessMainViewModel()) {
        val navController: NavHostController = rememberNavController()
        NavHost(navController = navController, startDestination = "home") {
            composable("home") {
                HomeScreen(
                    viewModel,
                    onStartChallenge = { navController.navigate("challenge") }
                )
            }

            composable(route = "challenge") {
                LivenessScreen(
                    stringResource(R.string.TOKEN),
                    viewModel,
                    onChallengeComplete = {
                        navController.navigate("results") {
                            popUpTo("challenge") {
                                inclusive = true
                            }
                        }

                    },
                    onBack = {
                        viewModel.clearSession()
                        navController.popBackStack()
                    }
                )
            }

            composable(route = "results") {
                ResultScreen(
                    viewModel,
                    onBack = {
                        viewModel.clearSession()
                        navController.popBackStack()
                    }
                )
            }
        }
    }
}